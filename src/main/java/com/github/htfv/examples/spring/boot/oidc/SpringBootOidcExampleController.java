package com.github.htfv.examples.spring.boot.oidc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class SpringBootOidcExampleController {

    @GetMapping("/unprotected/hello")
    String unprotectedHello() {
        return "Hello World!";
    }

    @GetMapping("/unprotected/exception")
    String unprotectedException() {
        throw new RuntimeException("Hello World!");
    }

    @GetMapping("/protected/hello")
    String protectedHello() {
        return "Hello Protected World!";
    }

    @GetMapping("/protected/exception")
    String protectedException() {
        throw new RuntimeException("Hello Protected World!");
    }
}
