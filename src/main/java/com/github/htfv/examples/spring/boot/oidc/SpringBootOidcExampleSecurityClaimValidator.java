package com.github.htfv.examples.spring.boot.oidc;

import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Objects;

class SpringBootOidcExampleSecurityClaimValidator implements OAuth2TokenValidator<Jwt> {

    private static final String EXAMPLE_CLAIM_NAME = "example-claim";
    private static final OAuth2Error EXAMPLE_CLAIM_ERROR = new OAuth2Error(
            "invalid_token",
            "example-claim is missing or has unexpected value",
            null);

    private final String exampleClaimValue;

    SpringBootOidcExampleSecurityClaimValidator(String exampleClaimValue) {
        this.exampleClaimValue = exampleClaimValue;
    }

    @Override
    public OAuth2TokenValidatorResult validate(Jwt jwt) {

        if (!Objects.equals(jwt.getClaims().get(EXAMPLE_CLAIM_NAME), exampleClaimValue)) {
            return OAuth2TokenValidatorResult.failure(EXAMPLE_CLAIM_ERROR);
        }

        return OAuth2TokenValidatorResult.success();
    }
}
