package com.github.htfv.examples.spring.boot.oidc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoderJwkSupport;

@EnableConfigurationProperties(SpringBootOidcExampleSecurityProperties.class)
@EnableWebSecurity
class SpringBootOidcExampleSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtDecoder jwtDecoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.oauth2Login()
                .disable();

        http.oauth2ResourceServer().jwt()
                .decoder(jwtDecoder);

        http.authorizeRequests()
                .mvcMatchers("/protected/**")
                .authenticated();
        http.authorizeRequests()
                .anyRequest()
                .permitAll();
    }

    @Bean
    JwtDecoder jwtDecoder(
            SpringBootOidcExampleSecurityClaimValidator securityClaimValidator,
            SpringBootOidcExampleSecurityProperties securityProperties) {

        NimbusJwtDecoderJwkSupport jwtDecoder = (NimbusJwtDecoderJwkSupport)
                JwtDecoders.fromOidcIssuerLocation(
                        securityProperties.getOidcIssuerUri());

        jwtDecoder.setJwtValidator(
                new DelegatingOAuth2TokenValidator<>(
                        securityClaimValidator,
                        JwtValidators.createDefaultWithIssuer(
                                securityProperties.getOidcIssuerUri())));

        return jwtDecoder;
    }

    @Bean
    SpringBootOidcExampleSecurityClaimValidator securityClaimValidator(
            SpringBootOidcExampleSecurityProperties securityProperties) {

        return new SpringBootOidcExampleSecurityClaimValidator(
                securityProperties.getExampleClaim());
    }
}
