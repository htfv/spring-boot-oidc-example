package com.github.htfv.examples.spring.boot.oidc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootOidcExampleApplication {

    public static void main(String... args) {
        SpringApplication.run(SpringBootOidcExampleApplication.class, args);
    }
}
