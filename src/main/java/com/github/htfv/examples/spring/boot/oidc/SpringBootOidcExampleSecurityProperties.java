package com.github.htfv.examples.spring.boot.oidc;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("example.security")
class SpringBootOidcExampleSecurityProperties {
    private String exampleClaim;
    private String oidcIssuerUri;

    public String getExampleClaim() {
        return exampleClaim;
    }

    public void setExampleClaim(String exampleClaim) {
        this.exampleClaim = exampleClaim;
    }

    public String getOidcIssuerUri() {
        return oidcIssuerUri;
    }

    public void setOidcIssuerUri(String oidcIssuerUri) {
        this.oidcIssuerUri = oidcIssuerUri;
    }
}
