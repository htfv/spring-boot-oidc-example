package com.github.htfv.examples.spring.boot.oidc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.client.endpoint.DefaultClientCredentialsTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2ClientCredentialsGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootOidcExampleApplicationTest {

    @Autowired
    private OAuth2AccessTokenResponseClient<OAuth2ClientCredentialsGrantRequest> clientCredentialsTokenResponseClient;

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void unprotectedHello_shouldReturnOk() throws Exception {
        mockMvc.perform(get("/unprotected/hello"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World!"));
    }

    @Test
    public void protectedHello_shouldReturnOk_whenAuthorized() throws Exception {

        ClientRegistration clientRegistration =
                clientRegistrationRepository.findByRegistrationId("example-keycloak");

        OAuth2ClientCredentialsGrantRequest clientCredentialsGrantRequest =
                new OAuth2ClientCredentialsGrantRequest(clientRegistration);

        OAuth2AccessTokenResponse tokenResponse =
                clientCredentialsTokenResponseClient.getTokenResponse(clientCredentialsGrantRequest);

        mockMvc.perform(
                get("/protected/hello")
                        .with(bearerToken(tokenResponse.getAccessToken().getTokenValue())))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Protected World!"));
    }

    private RequestPostProcessor bearerToken(String token) {
        return request -> {
            request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);
            return request;
        };
    }

    @TestConfiguration
    static class TestSecurityConfiguration {

        @Bean
        DefaultClientCredentialsTokenResponseClient clientCredentialsTokenResponseClient() {
            return new DefaultClientCredentialsTokenResponseClient();
        }
    }
}
